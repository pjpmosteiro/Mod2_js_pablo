/*
 * Aquí podéis hacer los ejercicios y
 * practicar Javascript!
 */

console.log("Probando, probando, 1, 2, 3!");
//02-BL2

//Ejercicio 1
/*
1;

//EJERCICIO 2
function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
delay(3000).then(() => alert("runs after 3 seconds"));

/////
function loadScript(src) {
  return new Promise(function(resolve, reject) {
    let script = document.createElement("script");
    script.src = src;

    script.onload = () => resolve(script);
    script.onerror = () => reject(new Error("Script load error: " + src));

    document.head.append(script);
  });
}

loadScript("./one.js")
  .then(function(script) {
    return loadScript("./two.js");
  })
  .then(function(script) {
    return loadScript("./three.js");
  })
  .then(function(script) {
    // use functions declared in scripts
    // to show that they indeed loaded
    one();
    two();
    three();
  });
*/
// Crear una funcion (num) que contenga una promesa de que si el numero es mayor de 10 se resuelva y si no, no. Llamar a la funcion y coger los datos o controlar los errores.

function compare(num) {
  return new Promise(function(resolve, reject) {
    if (num > 10) resolve("Bieeeen, sabes contar.....");
    else {
      return reject(new Error("Numero menor de 10... PALETO!"));
    }
  });
}

compare(4).then(result => alert(result), error => alert(error));
 
//Escribir dos funciones que usen promesas que puedas encadenar. La primera función pasarAMayus() que tomará un array de palabras y las pondrá en mayúsculas y la segunda función ordenarPalabras() que las ordenará alfabéticamente.

//Si el array contiene datos que no son strings debería lanzar un error.

function pasarAMayus(word) {
    return new 
}

function ordenarPalabras() {
    
}