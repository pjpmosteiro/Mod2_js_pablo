/*
 * Aquí podéis hacer los ejercicios y
 * practicar Javascript!
 */

console.log("Probando, probando, 1, 2, 3!");

//Array methods
// Ejercicio 1

function camelize(str) {
  let arr = str.split("-");
  let result = arr.map(function(item, index, array) {
    if (index > 0) {
      return item.charAt(0).toUpperCase() + item.slice(1);
    } else {
      return item;
    }
  });
  result = result.join("");
  return result;
}

alert(camelize("cara-de-sapo"));

// ejercicio 2

let arr = [5, 3, 8, 1];
